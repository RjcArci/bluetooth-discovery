package com.muy.bluetoothdiscovery.bluetooth

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.muy.bluetoothdiscovery.R
import com.muy.bluetoothdiscovery.core.ApplicationConstants
import org.jetbrains.anko.toast
import java.time.LocalDateTime

class MainActivity : AppCompatActivity() {

    private val txtAttendance by lazy<TextView> {
        findViewById(R.id.txt_main_attendance)
    }

    private val btnActivate by lazy<Button> {
        findViewById(R.id.btn_main_activate)
    }

    private val btnCancel by lazy<Button> {
        findViewById(R.id.btn_main_cancel)
    }

    var mBluetoothAdapter: BluetoothAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureAdapter()
        createClickListeners()
    }

    private fun createClickListeners() {
        btnActivate.setOnClickListener {
            keepDiscovery()
            btnActivate.isClickable = false
            btnActivate.setBackgroundColor(Color.GRAY)
        }

        btnCancel.setOnClickListener {
            stopDiscovery()
        }
    }

    private fun configureAdapter() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_support), Toast.LENGTH_SHORT).show()
            return
        }
    }

    private fun validateBluetooth() {
        if (!mBluetoothAdapter!!.isEnabled) {
            val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBluetoothIntent, ApplicationConstants.BLUETOOTH_PERMISSION)
        }
    }

    private fun keepDiscovery() {

        validateBluetooth()
        mBluetoothAdapter!!.startDiscovery()

        btnCancel.isClickable = true
        btnCancel.setBackgroundColor(Color.RED)
        checkAttendance()
    }

    private fun stopDiscovery() {
        if (mBluetoothAdapter!!.isEnabled) {
            mBluetoothAdapter!!.cancelDiscovery()
            btnCancel.isClickable = false
            btnCancel.setBackgroundColor(Color.GRAY)
        }
    }

    private fun checkAttendance() {
        val TAG = "TAG"
        // Instantiate the cache
        val cache = DiskBasedCache(cacheDir, 1024 * 1024) // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())

        // Instantiate the RequestQueue with the cache and network. Start the queue.
        val requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        val ldt = LocalDateTime.now()
        val date = "" + ldt.dayOfMonth + "-" + ldt.minusMonths(1).monthValue + "-" + ldt.year
        val url =
            "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + date + "/students/201325828%20"
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener { response ->
                txtAttendance.visibility = View.VISIBLE
                txtAttendance.setText("" + txtAttendance.text + date)
                stopDiscovery()
                mBluetoothAdapter!!.disable()
                requestQueue.cancelAll(TAG)
                requestQueue.stop()
            }, Response.ErrorListener { error ->
                toast("ATTENDANCE NOT FOUND")
            }
        )
        jsonObjectRequest.tag = TAG
        requestQueue.add(jsonObjectRequest)

    }

    override fun onStop() {
        super.onStop()
        mBluetoothAdapter!!.disable()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ApplicationConstants.BLUETOOTH_PERMISSION) {
            if (resultCode == Activity.RESULT_OK) {
                if (mBluetoothAdapter!!.isEnabled) {
                    toast("Bluetooth is enabled")
                }
            }
        }
    }
}
